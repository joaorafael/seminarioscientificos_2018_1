package br.com.mauda.seminario.cientificos.model;

public interface IdentifierInterface {

    public Long getId();

    public void setId(Long id);
}
